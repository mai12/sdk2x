

# -------------------  现在在 outdir 目录  ------------------
cp  -r crypto-config _crypto-config_
../package/bin/gm1.4.1/cryptogen   generate --config=./crypto-config-3.yaml
cp -r crypto-config/peerOrganizations/org3.example.com/ _crypto-config_/peerOrganizations/
rm -rf crypto-config
mv _crypto-config_ crypto-config
mkdir ./channel-artifacts
../package/bin/gm1.4.1/configtxgen -printOrg Org3MSP > ./channel-artifacts/org3.json
mkdir -p org3-artifacts/crypto-config/

export ORDERER_CA=./org3-artifacts/crypto-config/ordererOrganizations/example.com/tlsca/tlsca.example.com-cert.pem

export CHANNEL_NAME=$1


cd Admin@peer0.org1.example.com
# -------------------  现在在 Admin@peer0.org1.example.com 目录  ------------------
./peer.sh channel fetch config config_block.pb -o orderer.example.com:7050 -c $CHANNEL_NAME --tls --cafile ./tlsca.example.com-cert.pem

../../package/bin/gm1.4.1/configtxlator proto_decode --input config_block.pb --type common.Block | jq .data.data[0].payload.data.config > config.json

jq -s '.[0] * {"channel_group":{"groups":{"Application":{"groups": {"Org3MSP":.[1]}}}}}' config.json ../channel-artifacts/org3.json > modified_config.json

../../package/bin/gm1.4.1/configtxlator proto_encode --input config.json --type common.Config --output config.pb

../../package/bin/gm1.4.1/configtxlator proto_encode --input modified_config.json --type common.Config --output modified_config.pb

../../package/bin/gm1.4.1/configtxlator compute_update --channel_id $CHANNEL_NAME --original config.pb --updated modified_config.pb --output org3_update.pb

../../package/bin/gm1.4.1/configtxlator proto_decode --input org3_update.pb --type common.ConfigUpdate | jq . > org3_update.json

echo '{"payload":{"header":{"channel_header":{"channel_id":"'$CHANNEL_NAME'", "type":2}},"data":{"config_update":'$(cat org3_update.json)'}}}' | jq . > org3_update_in_envelope.json


../../package/bin/gm1.4.1/configtxlator proto_encode --input org3_update_in_envelope.json --type common.Envelope --output org3_update_in_envelope.pb

./peer.sh channel signconfigtx -f org3_update_in_envelope.pb

cp  org3_update_in_envelope.pb  ../Admin@peer0.org2.example.com
# -------------------  现在在 Admin@peer0.org2.example.com 目录  ------------------
cd ../Admin@peer0.org2.example.com

./peer.sh channel update -f org3_update_in_envelope.pb -c $CHANNEL_NAME -o orderer.example.com:7050 --tls --cafile ./tlsca.example.com-cert.pem

cd ../


# -------------------  现在在 outdir 目录  ------------------
# ----         创建 Admin@peer0.org3.example.com         ----


mkdir Admin@peer0.org3.example.com

cp ./Admin@peer0.org1.example.com/core.yaml ./Admin@peer0.org3.example.com

cp ./Admin@peer0.org1.example.com/peer ./Admin@peer0.org3.example.com

cp ./Admin@peer0.org1.example.com/tlsca.example.com-cert.pem ./Admin@peer0.org3.example.com

cp -r crypto-config/peerOrganizations/org3.example.com/users/Admin@org3.example.com/ ./Admin@peer0.org3.example.com

cat > ./Admin@peer0.org3.example.com/peer.sh << EOF

#!/bin/bash

export FABRIC_CFG_PATH=\`pwd\`
export CORE_PEER_ADDRESS=peer0.org3.example.com:7051
export CORE_PEER_LOCALMSPID="Org3MSP"
export CORE_PEER_MSPCONFIGPATH=Admin@org3.example.com/msp

export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_TLS_CERT_FILE=Admin@org3.example.com/tls/client.crt
export CORE_PEER_TLS_KEY_FILE=Admin@org3.example.com/tls/client.key
export CORE_PEER_TLS_ROOTCERT_FILE=Admin@org3.example.com/tls/ca.crt

export FABRIC_LOGGING_SPEC=INFO

./peer  \$*

EOF

chmod 777  ./Admin@peer0.org3.example.com/peer.sh

cp ./Admin@peer0.org1.example.com/$CHANNEL_NAME.block  ./Admin@peer0.org3.example.com/

# -------------------  现在在 outdir 目录  ------------------
# ----            创建 peer0.org3.example.com            ----

mkdir peer0.org3.example.com

cp -r ./crypto-config/peerOrganizations/org3.example.com/peers/peer0.org3.example.com/msp/ ./peer0.org3.example.com

cp -r ./crypto-config/peerOrganizations/org3.example.com/peers/peer0.org3.example.com/tls/ ./peer0.org3.example.com

cp ./peer0.org1.example.com/core.yaml ./peer0.org3.example.com/


cat > ./peer0.org3.example.com/start.sh << EOF


export FABRIC_LOGGING_SPEC=INFO
export CORE_PEER_FILESYSTEMPATH=data
export CORE_PEER_MSPCONFIGPATH=msp
export CORE_LEDGER_STATE_STATEDATABASE=goleveldb
export CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=:5984
export CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=admin
# export CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=$1

export CORE_PEER_ID=peer0.org3.example.com
# export CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=?
# export CORE_PEER_NETWORKID=dev
export CORE_PEER_ADDRESS=peer0.org3.example.com:7051
export CORE_PEER_LISTENADDRESS=0.0.0.0:7051
export CORE_PEER_CHAINCODEADDRESS=peer0.org3.example.com:7052
export CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:7052
export CORE_PEER_GOSSIP_USELEADERELECTION=true
export CORE_PEER_GOSSIP_ORGLEADER=false
export CORE_PEER_PROFILE_ENABLED=false
export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_TLS_CERT_FILE=tls/server.crt
export CORE_PEER_TLS_KEY_FILE=tls/server.key
export CORE_PEER_TLS_ROOTCERT_FILE=tls/ca.crt
export CORE_PEER_TLS_CLIENTROOTCAS_FILES=[tls/ca.crt]
export CORE_PEER_LOCALMSPID=Org3MSP
export CORE_PEER_GOSSIP_BOOTSTRAP=127.0.0.1:7051
export CORE_PEER_GOSSIP_ENDPOINT=peer0.org3.example.com:7051
export CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer0.org3.example.com:7051
export CORE_OPERATIONS_LISTENADDRESS=peer0.org3.example.com:9443
#export CORE_CHAINCODE_BUILDER=fuchain/fabric-ccenv:amd64-1.4.1
#export CORE_CHAINCODE_GOLANG_RUNTIME=fuchain/fabric-baseos:amd64-0.4.15
#export CORE_VM_ENDPOINT=http://192.168.1.82:2375
##export CORE_VM_DOCKER_ATTACHSTDOUT=true


nohup ./peer node start >> peer0.org3.example.com.log 2>&1 &

EOF

chmod 777 ./peer0.org3.example.com/start.sh


# 获取 channel 原始配置块
./peer.sh channel fetch 0 mychannel.block -o orderer.example.com:7050 -c mychannel --tls --cafile ./tlsca.example.com-cert.pem
# 获取 channel 最新配置块。可以看到加入的org3的信息
./peer.sh channel fetch config mychannel_new_c.block -o orderer.example.com:7050 -c mychannel --tls --cafile ./tlsca.example.com-cert.pem

# 这样可以加入通道
./peer channel join -b ${MYCHNAME}.block

# peer 参数
export CORE_PEER_GOSSIP_USELEADERELECTION=true
export CORE_PEER_GOSSIP_ORGLEADER=false