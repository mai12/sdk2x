package com.laiyuezs.sdk2x;

import com.alibaba.fastjson.JSONObject;
import org.hyperledger.fabric.sdk.ChaincodeCollectionConfiguration;
import org.hyperledger.fabric.sdk.Channel;
import org.hyperledger.fabric.sdk.HFClient;
import org.hyperledger.fabric.sdk.LifecycleChaincodeEndorsementPolicy;
import org.hyperledger.fabric.sdk.exception.ChaincodeCollectionConfigurationException;
import org.hyperledger.fabric.sdk.exception.ChaincodeEndorsementPolicyParseException;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.ProposalException;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class TestSnapshot {
    String cryptoConfig = "/home/ct/Workspace/onekey/outdir";
    // 背书策略地址
    String E_P_PATH = "/home/ct/Workspace/golang/src/ly/chaincode/snapshot/chaincodeendorsementpolicyAllMembers.yaml";
    // 私有数据集地址 collections_config.yaml
    String C_C_PATH = "/home/ct/Workspace/golang/src/ly/chaincode/snapshot/collections_config.yaml";

    String chaindcode_l = "snapshoted02_l";
    String chaincode_n="snapshoted02";
    @Test
    void lifecycleInstallChaincode() throws Exception {
        // OrgManage  om = new OrgManage("","example.com");
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        //Peer peer =;
        OrgManage.UserContext user = om.newUser("Admin", "Org1MSP", "org1");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));

        om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        //    om.channelAddPeer(om.newPeer("peer0", "org2", "peer0.org2.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();
        System.out.println(channel.queryBlockchainInfo().getHeight());
        // /home/ct/Workspace/golang/src/ly/chaincode/marbles02_private/collections_config.json
        // /home/ct/Workspace/golang/src/ly/chaincode/snapshot
        String packageID = ChaincodeLifecycle.lifecycleInstallChaincode(
                channel,
                hfClient,
                chaindcode_l, // 切记标签不能和链码名字一样，一样就报错
                "/home/ct/Workspace/golang",
                "ly/chaincode/snapshot",
                "/home/ct/Workspace/golang/src/ly/chaincode/snapshot"
        );
//        ChaincodeLifecycle.
        // 如果审批出现mod错误，就在本地执行go mod vendor
        System.out.println("------> package ID : " + packageID);
        // sdk4jfabcar_1:ec5f481744ae5fafcc43841334c20c2c2c21967aa8a11529f5c017878d7e0218
        // sdk4jfabcar0_1:ff8d183ff19fe352e0cb35590c9afa1b6196b90e3799303a43ba329cd0f43c63
        // marbles02_private_1:df39a69bc994b80631a5dfe7af94b72593349d7896405082ff0c8b162987ab58
        // marbles020_private_1:ba717becad86cad20f7517ebab69b9379f5409eb8908634d28862fcbcf553fe2
        // snapshot:59ac35f33c6199626df4c97594441a09cd7ba1c880ca84da875fe5211916fdbe
        // snapshoted_l:f777721174bae3bc07d9b5853f9efd619717de4ed2e9dabec0ae878af5285a7a
        // snapshoted01_l:3fcae5abab5f2e8fb648583286072d54327058901bae4f3f2b85cf3ed0a3f6d1
    }

    @Test
    void lifecycleApproveChaincode() throws Exception {
        // OrgManage  om = new OrgManage("","example.com");
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        //Peer peer =;
        OrgManage.UserContext user = om.newUser("Admin", "Org1MSP", "org1");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));

        //   om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();
        String packageID = "snapshoted02_l:b41012bc874d4f65f4372d1ee9192a125edf9066a4bef1014e80e555f5c84507";
        // //Org1 also creates the endorsement policy for the chaincode. // also known as validationParameter !
        LifecycleChaincodeEndorsementPolicy chaincodeEndorsementPolicy = LifecycleChaincodeEndorsementPolicy.fromSignaturePolicyYamlFile(Paths.get(E_P_PATH));
        ChaincodeCollectionConfiguration chaincodeCollectionConfiguration = ChaincodeCollectionConfiguration.fromYamlFile(new File(C_C_PATH));
        String txid = ChaincodeLifecycle.lifecycleApproveChaincode(
                channel,
                hfClient,
                packageID,
                chaincode_n,

                "0.1",
                chaincodeCollectionConfiguration,
                chaincodeEndorsementPolicy,
                1 // 失败后要+1,从1 开始
        );
        System.out.println(txid);
        // 0db8e8d67c2e61b5a8346740c3417e99ed415ff026af9ed196b6402c28b6e6b4
    }

    @Test
    void lifecycleCommitChaincode() throws Exception {

        // OrgManage  om = new OrgManage("","example.com");
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        //Peer peer =;
        OrgManage.UserContext user = om.newUser("Admin", "Org1MSP", "org1");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));

        om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        //  om.channelAddPeer(om.newPeer("peer0", "org2", "peer0.org2.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();
        //   String packageID = "sdk4jfabcar_1:ec5f481744ae5fafcc43841334c20c2c2c21967aa8a11529f5c017878d7e0218";

        // //Org1 also creates the endorsement policy for the chaincode. // also known as validationParameter !
        LifecycleChaincodeEndorsementPolicy chaincodeEndorsementPolicy = LifecycleChaincodeEndorsementPolicy.fromSignaturePolicyYamlFile(Paths.get(E_P_PATH));
        ChaincodeCollectionConfiguration chaincodeCollectionConfiguration = ChaincodeCollectionConfiguration.fromYamlFile(new File(C_C_PATH));

        String txid = ChaincodeLifecycle.lifecycleCommitChaincode(
                channel,
                hfClient,
                chaincode_n,
                "0.1",
                true,
                chaincodeCollectionConfiguration,
                chaincodeEndorsementPolicy,
                1 // 失败后加 1
        );
        System.out.println(txid);
    }

    @Test
    void lifecycleInitChaincode() throws Exception {

        // OrgManage  om = new OrgManage("","example.com");
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        //Peer peer =;
        OrgManage.UserContext user = om.newUser("Admin", "Org1MSP", "org1");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));

        om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();
        //  String packageID = "sdk4jfabcar_1:ec5f481744ae5fafcc43841334c20c2c2c21967aa8a11529f5c017878d7e0218";

        String txid = ChaincodeLifecycle.lifecycleInitChaincode(channel, hfClient, chaincode_n, user, "init");

        System.out.println(txid);
    }

    @Test
    void lifecycleInitChaincodeT() throws Exception {

        // OrgManage  om = new OrgManage("","example.com");
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        //Peer peer =;
        OrgManage.UserContext user = om.newUser("Admin", "Org1MSP", "org1");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));

        om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();
        //  String packageID = "sdk4jfabcar_1:ec5f481744ae5fafcc43841334c20c2c2c21967aa8a11529f5c017878d7e0218";
       String mjson_str ="{\n  \"createDate\": 20190713,\n  \"createTime\": 120224,\n  \"updateDate\": 20211122,\n  \"updateTime\": 80702,\n  \"status\": 0,\n  \"operatorId\": \"USUSERAP201906031703023816000049\",\n  \"id\": \"ORLOGIAP201907131202023813000001#1637638065803\",\n  \"vehicleId\": \"ASVEHIAP201906031708023816000048\",\n  \"numberPlate\": \"鲁QS8901\",\n  \"goodsType\": \"ASGOOTAP201906021855202213000001\",\n  \"secondGoodsId\": \"ASGOOTRT201911181454100751000018\",\n  \"firstGoodsId\": \"ASGOOTAP201911181026010719000001\",\n  \"goodsTypeName\": \"外购废钢（通级）\",\n  \"destination\": \"山东省-日照市-岚山区 沿海路日照钢铁控股集团有限公司东1门\",\n  \"destinationPoint\": \"119.38821851569789,35.16573695168669\",\n  \"beginTime\": 1562990544779,\n  \"endTime\": 1563015653856,\n  \"type\": \"3\",\n  \"distance\": 54.726935639027,\n  \"deviceId\": \"fe6c310cecdd7aca0\",\n  \"logisticsConfigId\": \"ASLPCHAP201908071113100751000001\",\n  \"nature\": \"car\",\n  \"userId\": \"USUSERAP201906031703023816000049\",\n  \"userName\": \"赵树鲁\",\n  \"mobile\": \"13508995522\",\n  \"steelId\": \"USUSERAP201905051256220229000199\",\n  \"steelName\": \"日照钢铁控股集团有限公司\",\n  \"supplyCompany\": \"LX4589\",\n  \"supplyCompanyName\": \"山东中钢再生资源有限公司\",\n  \"supplier\": \"LX4710\",\n  \"supplierName\": \"史瑞东\",\n  \"distinguish\": \"BASIC\",\n  \"source\": \"basic\",\n  \"imageResources\": {\n    \"outVoucherUrl\": {\n      \"@c\": \".ImageResource\",\n      \"id\": \"ORBASROR201910101827685097179808\",\n      \"parentId\": \"ORLOGIAP201907131202023813000001\",\n      \"resourceKey\": \"outVoucherUrl\",\n      \"resourceValue\": \"OSOSSFAP201907131537202216000001\",\n      \"resourceType\": \"STR_ARRAY\"\n    },\n    \"dischargeVoucherUrl\": {\n      \"@c\": \".ImageResource\",\n      \"id\": \"ORBASROR201910101827685097269805\",\n      \"parentId\": \"ORLOGIAP201907131202023813000001\",\n      \"resourceKey\": \"dischargeVoucherUrl\",\n      \"resourceValue\": \"OSOSSFAP201907141534023813000001\",\n      \"resourceType\": \"STR_ARRAY\"\n    },\n    \"livePhotoId\": {\n      \"@c\": \".ImageResource\",\n      \"id\": \"ORBASROR201910101827685097304412\",\n      \"parentId\": \"ORLOGIAP201907131202023813000001\",\n      \"resourceKey\": \"livePhotoId\",\n      \"resourceValue\": \"OSOSSFAP201907141534202216000001\",\n      \"resourceType\": \"STR_ARRAY\"\n    },\n    \"weightPhotoUrl\": {\n      \"@c\": \".ImageResource\",\n      \"id\": \"ORBASROR201910101827685097225646\",\n      \"parentId\": \"ORLOGIAP201907131202023813000001\",\n      \"resourceKey\": \"weightPhotoUrl\",\n      \"resourceValue\": \"OSOSSFAP201907131202023813000001\",\n      \"resourceType\": \"STR_ARRAY\"\n    }\n  },\n  \"staticResources\": {\n    \"subsidy\": {\n      \"@c\": \".StaticResource\",\n      \"id\": \"ORBASROR201910101827685097091528\",\n      \"parentId\": \"ORLOGIAP201907131202023813000001\",\n      \"resourceKey\": \"subsidy\",\n      \"resourceValue\": \"10.00\",\n      \"resourceType\": \"BIG_DECIMAL\"\n    },\n    \"actualTare\": {\n      \"@c\": \".StaticResource\",\n      \"id\": \"ORBASROR202008241532202174000031\",\n      \"parentId\": \"ORLOGIAP201907131202023813000001\",\n      \"resourceKey\": \"actualTare\",\n      \"resourceValue\": \"15.64\",\n      \"resourceType\": \"STRING\"\n    },\n    \"storageNetWeight\": {\n      \"@c\": \".StaticResource\",\n      \"id\": \"ORBASROR202008241532202174000027\",\n      \"parentId\": \"ORLOGIAP201907131202023813000001\",\n      \"resourceKey\": \"storageNetWeight\",\n      \"resourceValue\": \"38.9\",\n      \"resourceType\": \"STRING\"\n    },\n    \"materialType\": {\n      \"@c\": \".StaticResource\",\n      \"id\": \"ORBASROR202008241532202174000029\",\n      \"parentId\": \"ORLOGIAP201907131202023813000001\",\n      \"resourceKey\": \"materialType\",\n      \"resourceValue\": \"外购废钢（通级）\",\n      \"resourceType\": \"STRING\"\n    },\n    \"pureWeight\": {\n      \"@c\": \".StaticResource\",\n      \"id\": \"ORBASROR201910101827685097026109\",\n      \"parentId\": \"ORLOGIAP201907131202023813000001\",\n      \"resourceKey\": \"pureWeight\",\n      \"resourceValue\": \"39.2\",\n      \"resourceType\": \"DOUBLE\"\n    },\n    \"baidu\": {\n      \"@c\": \".StaticResource\",\n      \"id\": \"ORBASROR202005301733203410000004\",\n      \"parentId\": \"ORLOGIAP201907131202023813000001\",\n      \"resourceKey\": \"baidu\",\n      \"resourceValue\": \"LOTRAILO202005301733203413000004\",\n      \"resourceType\": \"STRING\"\n    },\n    \"baiduPath\": {\n      \"@c\": \".StaticResource\",\n      \"id\": \"ORBASROR202005301733203410000006\",\n      \"parentId\": \"ORLOGIAP201907131202023813000001\",\n      \"resourceKey\": \"baiduPath\",\n      \"resourceValue\": \"/app/logs/point/2020/0530/ORLOGIAP201907131202023813000001.json\",\n      \"resourceType\": \"STRING\"\n    },\n    \"actualGrossWeight\": {\n      \"@c\": \".StaticResource\",\n      \"id\": \"ORBASROR202008241532202174000032\",\n      \"parentId\": \"ORLOGIAP201907131202023813000001\",\n      \"resourceKey\": \"actualGrossWeight\",\n      \"resourceValue\": \"54.54\",\n      \"resourceType\": \"STRING\"\n    },\n    \"businessno\": {\n      \"@c\": \".StaticResource\",\n      \"id\": \"ORBASROR201910101827685097135290\",\n      \"parentId\": \"ORLOGIAP201907131202023813000001\",\n      \"resourceKey\": \"businessno\",\n      \"resourceValue\": \"\",\n      \"resourceType\": \"STRING\"\n    },\n    \"netWeightDate\": {\n      \"@c\": \".StaticResource\",\n      \"id\": \"ORBASROR202008241532202174000028\",\n      \"parentId\": \"ORLOGIAP201907131202023813000001\",\n      \"resourceKey\": \"netWeightDate\",\n      \"resourceValue\": \"1563015653000\",\n      \"resourceType\": \"STRING\"\n    },\n    \"bdistance\": {\n      \"@c\": \".StaticResource\",\n      \"id\": \"ORBASROR202005301733203410000005\",\n      \"parentId\": \"ORLOGIAP201907131202023813000001\",\n      \"resourceKey\": \"bdistance\",\n      \"resourceValue\": \"58.04\",\n      \"resourceType\": \"DOUBLE\"\n    },\n    \"deduction\": {\n      \"@c\": \".StaticResource\",\n      \"id\": \"ORBASROR202008241532202174000030\",\n      \"parentId\": \"ORLOGIAP201907131202023813000001\",\n      \"resourceKey\": \"deduction\",\n      \"resourceValue\": \"0.0\",\n      \"resourceType\": \"STRING\"\n    },\n    \"actualDestination\": {\n      \"@c\": \".StaticResource\",\n      \"id\": \"ORBASROR202008241532202174000033\",\n      \"parentId\": \"ORLOGIAP201907131202023813000001\",\n      \"resourceKey\": \"actualDestination\",\n      \"resourceValue\": \"山东省-日照市-岚山区 沿海路日照钢铁控股集团有限公司东123门\",\n      \"resourceType\": \"STRING\"\n    }\n  },\n  \"dynamicResources\": {\n    \"actualNetWeight\": {\n      \"@c\": \".DynamicResource\",\n      \"id\": \"ORBASROR202008241532202174000034\",\n      \"parentId\": \"ORLOGIAP201907131202023813000001\",\n      \"resourceKey\": \"actualNetWeight\",\n      \"resourceValue\": \"38.9\",\n      \"resourceType\": \"STRING\"\n    }\n  },\n  \"netWeight\": 39.2,\n  \"owner\": \"USUSERAP201906031703023816000049\"\n}";

        // String txid = ChaincodeLifecycle.lifecycleInitChaincode(channel, hfClient, "snapshoted", user, "init");
        HashMap<String,byte[]> map = new HashMap<>();
        map.put("snapshotType", "logistics".getBytes());
        map.put("logistics", mjson_str.getBytes());

       JSONObject jsonObject = Cc.invoke(hfClient, channel, chaincode_n,"updateSnapshot",map);
        System.out.println(jsonObject);
    }

}
