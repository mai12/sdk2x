package com.laiyuezs.sdk2x.console.templates;

import com.esotericsoftware.yamlbeans.YamlReader;
import com.laiyuezs.sdk2x.console.templates.organizationsTemplate.*;
import org.junit.jupiter.api.Test;
import org.yaml.snakeyaml.Yaml;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class OrganizationsTemplateTest {

    @Test
    void writeFile() throws IOException {

        OrganizationsTemplate ot = new OrganizationsTemplate();
        Organization og = new Organization();
        og.setName("Org4MSP");
        og.setID("Org4MSP");
        og.setMSPDir("/home/ct/Workspace/java/sdk2x/src/test/fix/organizations/peerOrganizations/org4.example.com/msp");
        Policies pl = new Policies(new Readers(Rule.SIGNATURE,Rule.adminPeerClient(og.ID)),
                new Writers(Rule.SIGNATURE,Rule.adminClient(og.ID)),
                new Admins(Rule.SIGNATURE,Rule.admin(og.ID)));

      //  System.out.println(pl.Writers.Rule);
        og.setPolicies( pl);
        og.addAnchorPeer(new AnchorPeer("peer0.org4.example.com",7051));


        ot.addOrganization(og);
        ot.writeFile(Paths.get("/home/ct/Workspace/java/sdk2x/src/test/fix","configtx.yaml"));


    }

    /*

      //  /home/ct/Workspace/onekey/package/bin/2.2.1/configtxgen -printOrg Org4MSP > ./org4.json

 /home/ct/Workspace/onekey/package/bin/2.2.1/configtxlator proto_decode --input config_block.pb --type common.Config > config.json

jq -s '.[0] * {"channel_group":{"groups":{"Application":{"groups": {"Org4MSP":.[1]}}}}}' config.json ./org4.json > modified_config.json



     */
}