package com.laiyuezs.sdk2x.console.templates;

import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;
import com.laiyuezs.sdk2x.console.templates.peerOrgTemplate.PeerOrg;
import org.junit.jupiter.api.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class PeerOrgsTemplateTest {

    @Test
    void add() throws IOException {
        PeerOrgsTemplate peerOrgsTemplate = new PeerOrgsTemplate();

        PeerOrg peerOrg = new PeerOrg();


        peerOrg.setName("org4")
                .setDomain("org4.example.com")
                .setEnableNodeOUs(true)
                .setTemplate(2)
                .setCA("CN", "BJ", "LY")
                .setUsers(1);

        peerOrgsTemplate.add(peerOrg);

       // YamlReader yamlReader = new YamlReader(peerOrgsTemplate)
//
//        YamlWriter yamlWriter = new YamlWriter(new FileWriter("/home/ct/Workspace/java/sdk2x/src/test/fix/output.yaml"));
//
//        yamlWriter.write(peerOrgsTemplate);
//        yamlWriter.close();

        peerOrgsTemplate.writeFile(Paths.get("/home/ct/Workspace/java/sdk2x/src/test/fix/","output.yaml"));
    }

    /*
    /home/ct/Workspace/onekey/package/bin/2.2.1/cryptogen \
     generate  --config=/home/ct/Workspace/java/sdk2x/src/test/fix/output.yaml \
     --output="organizations"
     */
}