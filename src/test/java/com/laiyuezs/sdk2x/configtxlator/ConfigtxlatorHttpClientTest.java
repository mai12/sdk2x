package com.laiyuezs.sdk2x.configtxlator;

import com.laiyuezs.sdk2x.InUtils;
import org.hyperledger.fabric.sdk.UpdateChannelConfiguration;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;

import static org.junit.jupiter.api.Assertions.*;

class ConfigtxlatorHttpClientTest {

    @Test
    void computeUpdate() throws Exception{

        ConfigtxlatorHttpClient configtxlatorHttpClient = new ConfigtxlatorHttpClient();

        String channelName = "mychannel";
        // 读取两个文件
        byte[] original = InUtils.toByteArray("/home/ct/Workspace/java/sdk2x/src/test/fix/config.pb");
        byte[] updated = InUtils.toByteArray("/home/ct/Workspace/java/sdk2x/src/test/fix/modified_config.pb");

        // org4_update.pb
        byte[] orgUpdatePb =  configtxlatorHttpClient.computeUpdate("47.105.36.27:7059",channelName,original,updated);

        InUtils.writeStream(new ByteArrayInputStream(orgUpdatePb),"/home/ct/Workspace/java/sdk2x/src/test/fix/org4_update_test.pb");

        UpdateChannelConfiguration updateChannelConfiguration = new UpdateChannelConfiguration(orgUpdatePb);
    }
}