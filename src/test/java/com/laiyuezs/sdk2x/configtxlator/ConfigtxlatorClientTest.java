package com.laiyuezs.sdk2x.configtxlator;

import com.alibaba.fastjson.JSONObject;
import com.dtflys.forest.config.ForestConfiguration;
import com.laiyuezs.sdk2x.InUtils;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class ConfigtxlatorClientTest {

    @Test
    void uploadByteArrayMap() throws Exception {

        // 实例化Forest配置对象
        ForestConfiguration configuration = ForestConfiguration.configuration();

        // 通过Forest配置对象实例化Forest请求接口
        ConfigtxlatorClient myClient = configuration.createInstance(ConfigtxlatorClient.class);

        // 读取两个文件
        byte[] original = InUtils.readStream(new FileInputStream("/home/ct/Workspace/java/sdk2x/src/test/fix/config.pb")).getBytes();
        byte[] updated = InUtils.readStream(new FileInputStream("/home/ct/Workspace/java/sdk2x/src/test/fix/modified_config.pb")).getBytes();
        HashMap<String,byte[]> fileMap = new HashMap<>();
        fileMap.put("original",original);
        fileMap.put("updated",updated);
        // 调用Forest请求接口，并获取响应返回结果
        byte[] orgUpdate = myClient.computeUpdate("47.105.36.27:7059","mychannel",fileMap );

        // 写入文件检测

        InUtils.writeStream(new ByteArrayInputStream(orgUpdate),"/home/ct/Workspace/java/sdk2x/src/test/fix/org4update.pb");
    }
}