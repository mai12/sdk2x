package com.laiyuezs.sdk2x;

import com.alibaba.fastjson.JSONObject;
import com.dtflys.forest.config.ForestConfiguration;
import com.google.protobuf.Message;
import com.google.protobuf.util.JsonFormat;
import com.laiyuezs.sdk2x.configtxlator.ConfigtxlatorClient;
import com.laiyuezs.sdk2x.configtxlator.ConfigtxlatorHttpClient;
import com.laiyuezs.sdk2x.console.templates.organizationsTemplate.Rule;
import org.hyperledger.fabric.protos.common.Configtx;
import org.hyperledger.fabric.sdk.*;
import org.hyperledger.fabric.protos.common.Common.Block;
import org.hyperledger.fabric.sdk.exception.*;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;


import static org.junit.jupiter.api.Assertions.*;

/**
 * @author : 州长在手 2021/7/8 上午9:43
 */
class ChaincodeLifecycleTest {

    //String cryptoConfig = "/home/ct/workspace/fabric2.2/fabric-samples/lucia";
    String cryptoConfig = "/home/ct/Workspace/onekey/outdir";

    @Test
    void lifecycleInstallChaincode() throws InvalidArgumentException, ProposalException, IOException {
        // OrgManage  om = new OrgManage("","example.com");
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        //Peer peer =;
        OrgManage.UserContext user = om.newUser("Admin", "Org1MSP", "org1");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));

        om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        //    om.channelAddPeer(om.newPeer("peer0", "org2", "peer0.org2.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();
        System.out.println(channel.queryBlockchainInfo().getHeight());
        // /home/ct/Workspace/golang/src/ly/chaincode/marbles02_private/collections_config.json
        String packageID = ChaincodeLifecycle.lifecycleInstallChaincode(
                channel,
                hfClient,
                "marbles01_private",
                "/home/ct/go",
                "github.com/hyperledger/fabric-samples/chaincode/marbles02_private/go",
                "/home/ct/Workspace/golang/src/ly/chaincode/marbles02_private/go"
        );
//        ChaincodeLifecycle.
        // 如果审批出现mod错误，就在本地执行go mod vendor
        System.out.println("------> package ID : " + packageID);
        // sdk4jfabcar_1:ec5f481744ae5fafcc43841334c20c2c2c21967aa8a11529f5c017878d7e0218
        // sdk4jfabcar0_1:ff8d183ff19fe352e0cb35590c9afa1b6196b90e3799303a43ba329cd0f43c63
        // marbles02_private_1:df39a69bc994b80631a5dfe7af94b72593349d7896405082ff0c8b162987ab58
        // marbles020_private_1:ba717becad86cad20f7517ebab69b9379f5409eb8908634d28862fcbcf553fe2
    }

    @Test
    void lifecycleApproveChaincode() throws IOException, ChaincodeEndorsementPolicyParseException, ProposalException, InvalidArgumentException, ExecutionException, InterruptedException, TimeoutException, ChaincodeCollectionConfigurationException {
        // OrgManage  om = new OrgManage("","example.com");
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        //Peer peer =;
        OrgManage.UserContext user = om.newUser("Admin", "Org1MSP", "org1");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));

        //   om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();
        String packageID = "marbles01_private:86e953b1992110785cf15c6e88087c79358567722ab50878dab1112f28d0a9b2";
        // //Org1 also creates the endorsement policy for the chaincode. // also known as validationParameter !
        LifecycleChaincodeEndorsementPolicy chaincodeEndorsementPolicy = LifecycleChaincodeEndorsementPolicy.fromSignaturePolicyYamlFile(Paths.get("/home/ct/Workspace/golang/src/ly/chaincode/marbles02_private/chaincodeendorsementpolicyAllMembers.yaml"));
        ChaincodeCollectionConfiguration chaincodeCollectionConfiguration = ChaincodeCollectionConfiguration.fromYamlFile(new File("/home/ct/Workspace/golang/src/ly/chaincode/marbles02_private/collections_config.yaml"));
        String txid = ChaincodeLifecycle.lifecycleApproveChaincode(
                channel,
                hfClient,
                packageID,
                "marbles0101private",

                "0.1",
                chaincodeCollectionConfiguration,
                chaincodeEndorsementPolicy,
                1 // 失败后要+1,从1 开始
        );
        System.out.println(txid);
    }

    @Test
    void lifecycleCommitChaincode() throws InterruptedException, InvalidArgumentException, TimeoutException, ProposalException, ExecutionException, IOException, ChaincodeEndorsementPolicyParseException, ChaincodeCollectionConfigurationException {

        // OrgManage  om = new OrgManage("","example.com");
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        //Peer peer =;
        OrgManage.UserContext user = om.newUser("Admin", "Org1MSP", "org1");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));

        om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        //  om.channelAddPeer(om.newPeer("peer0", "org2", "peer0.org2.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();
        //   String packageID = "sdk4jfabcar_1:ec5f481744ae5fafcc43841334c20c2c2c21967aa8a11529f5c017878d7e0218";

        // //Org1 also creates the endorsement policy for the chaincode. // also known as validationParameter !
        LifecycleChaincodeEndorsementPolicy chaincodeEndorsementPolicy = LifecycleChaincodeEndorsementPolicy.fromSignaturePolicyYamlFile(Paths.get("/home/ct/IdeaProjects/fabric-sdk-java/src/test/fixture/" +
                "/sdkintegration/chaincodeendorsementpolicyAllMembers.yaml"));
        ChaincodeCollectionConfiguration chaincodeCollectionConfiguration = ChaincodeCollectionConfiguration.fromYamlFile(new File("/home/ct/go/src/github.com/hyperledger/fabric-samples/chaincode/marbles02_private/collections_config.yaml"));

        String txid = ChaincodeLifecycle.lifecycleCommitChaincode(
                channel,
                hfClient,
                "marbles0101private",
                "0.1",
                true,
                chaincodeCollectionConfiguration,
                chaincodeEndorsementPolicy,
                1 // 失败后加 1
        );
        System.out.println(txid);
    }

    @Test
    void lifecycleInitChaincode() throws InterruptedException, InvalidArgumentException, TimeoutException, ProposalException, ExecutionException {

        // OrgManage  om = new OrgManage("","example.com");
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        //Peer peer =;
        OrgManage.UserContext user = om.newUser("Admin", "Org1MSP", "org1");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));

        om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();
        //  String packageID = "sdk4jfabcar_1:ec5f481744ae5fafcc43841334c20c2c2c21967aa8a11529f5c017878d7e0218";

        String txid = ChaincodeLifecycle.lifecycleInitChaincode(channel, hfClient, "marbles0101private", user, "init");

        System.out.println(txid);
    }

    @Test
    void privateData() throws InterruptedException, InvalidArgumentException, ServiceDiscoveryException, ExecutionException, TimeoutException, ProposalException, UnsupportedEncodingException {
        // OrgManage  om = new OrgManage("","example.com");
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        //Peer peer =;
        OrgManage.UserContext user = om.newUser("Admin", "Org1MSP", "org1");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));

        om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();


        JSONObject json = new JSONObject();

        json.put("name", "marble5");
        json.put("color", "blue");
        json.put("size", 35);
        json.put("owner", "tom");
        json.put("price", 99);

        Map<String, byte[]> transientMap = new HashMap<>();
        System.out.println(json.toJSONString());

        transientMap.put("marble", json.toJSONString().getBytes());
        JSONObject js = Cc.invoke(hfClient, channel, "marbles0101private", "initMarble", transientMap);
        System.out.println(js);
    }

    @Test
    void invokeFabcar() throws InterruptedException, InvalidArgumentException, ServiceDiscoveryException, ExecutionException, TimeoutException, ProposalException {
        // OrgManage  om = new OrgManage("","example.com");
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        //Peer peer =;
        OrgManage.UserContext user = om.newUser("Admin", "Org1MSP", "org1");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));

        om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();
        JSONObject txidJson = Cc.invoke(hfClient, channel, "sdk4jfabcar0", "createCar", "CAR33", "Holden", "Barina", "brown", "zhangshulin");
        System.out.println(txidJson);
    }


    @Test
    void query() throws ProposalException, InvalidArgumentException {
        // OrgManage  om = new OrgManage("","example.com");
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        //Peer peer =;
        OrgManage.UserContext user = om.newUser("Admin", "Org1MSP", "org1");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));

        om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();

        //       JSONObject json = Cc.query(hfClient, channel, "sdk4jfabcar0", "queryCar", "CAR33");
        //      System.out.println(json);

        JSONObject json = Cc.query(om.getHfClient(), om.getChannel(), "marbles0101private", "readMarble", "marble3");
        System.out.println(json);
        json = Cc.query(om.getHfClient(), om.getChannel(), "marbles0101private", "readMarblePrivateDetails", "marble3");
        System.out.println(json);
    }


    @Test
    void updateChaincode() throws ProposalException, IOException, InvalidArgumentException, InterruptedException, ExecutionException, TimeoutException, ChaincodeEndorsementPolicyParseException, ChaincodeCollectionConfigurationException {

        // OrgManage  om = new OrgManage("","example.com");
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        //Peer peer =;
        OrgManage.UserContext user = om.newUser("Admin", "Org1MSP", "org1");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));

        om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();

        // ------------------------------- 安装新版本 -------------------------------
        String packageID = ChaincodeLifecycle.lifecycleInstallChaincode(
                channel,
                hfClient,
                "marbles020_private_2",
                "/home/ct/go",
                "github.com/hyperledger/fabric-samples/chaincode/marbles02_private/go",
                "/home/ct/Workspace/golang/src/ly/chaincode/marbles02_private/go"
        );
        // 如果审批出现mod错误，就在本地执行go mod vendor
        System.out.println("------> package ID : " + packageID);
        // ------------------------------- 审批  -------------------------------

        LifecycleChaincodeEndorsementPolicy chaincodeEndorsementPolicy = LifecycleChaincodeEndorsementPolicy.fromSignaturePolicyYamlFile(Paths.get("/home/ct/IdeaProjects/fabric-sdk-java/src/test/fixture/" +
                "/sdkintegration/chaincodeendorsementpolicy.yaml"));
        ChaincodeCollectionConfiguration chaincodeCollectionConfiguration = ChaincodeCollectionConfiguration.fromYamlFile(new File("/home/ct/Workspace/golang/src/ly/chaincode/marbles02_private/collections_config.yaml"));
        String txid = ChaincodeLifecycle.lifecycleApproveChaincode(
                channel,
                hfClient,
                packageID,
                "marbles020_private",

                "0.2",
                chaincodeCollectionConfiguration,
                chaincodeEndorsementPolicy,
                2 // 失败后要+1
        );

        System.out.println(txid);
        // ------------------------------- 提交  -------------------------------

        //   LifecycleChaincodeEndorsementPolicy chaincodeEndorsementPolicy = LifecycleChaincodeEndorsementPolicy.fromSignaturePolicyYamlFile(Paths.get("/home/ct/IdeaProjects/fabric-sdk-java/src/test/fixture/" +
        //           "/sdkintegration/chaincodeendorsementpolicy.yaml"));
        //   ChaincodeCollectionConfiguration chaincodeCollectionConfiguration = ChaincodeCollectionConfiguration.fromYamlFile(new File("/home/ct/Workspace/golang/src/ly/chaincode/marbles02_private/collections_config.yaml"));

        txid = ChaincodeLifecycle.lifecycleCommitChaincode(
                channel,
                hfClient,
                "marbles020_private",
                "0.2",
                true,
                chaincodeCollectionConfiguration,
                chaincodeEndorsementPolicy,
                2 // 失败后加 1
        );
        System.out.println(txid);


//        json = Cc.query(om.getHfClient(), om.getChannel(), "marbles020_private", "readMarble", "marble2");
//        System.out.println(json);
//        json = Cc.query(om.getHfClient(), om.getChannel(), "marbles020_private", "readMarblePrivateDetails", "marble2");
//        System.out.println(json);

    }


    @Test
    void channelConfig() throws Exception {
        // OrgManage  om = new OrgManage("","example.com");
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        //Peer peer =;
        OrgManage.UserContext user = om.newUser("Admin", "Org1MSP", "org1");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));

        om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();

        ConfigtxlatorHttpClient configtxlatorHttpClient = new ConfigtxlatorHttpClient();


        byte[] configPB = channel.getChannelConfigurationBytes();

        // configPB 写入文件检测
        InputStream isConfigPb = new ByteArrayInputStream(configPB);
        InUtils.writeStream(isConfigPb,"/home/ct/Workspace/java/sdk2x/src/test/fix/config.pb");

        // 添加的新组织
        String orgMSP = "Org4MSP";
        // 实例化Forest配置对象
        ForestConfiguration configuration = ForestConfiguration.configuration();

        // 通过Forest配置对象实例化Forest请求接口
        // ConfigtxlatorClient myClient = configuration.createInstance(ConfigtxlatorClient.class);


        // 调用Forest请求接口，并获取响应返回结果
        JSONObject configJson = configtxlatorHttpClient.decodeCommonConfig("47.105.36.27:7059", configPB);

        // 需要获取的数据
        byte[] jsonByte = configJson.toString().getBytes();

        InputStream inputStream = new ByteArrayInputStream(jsonByte);

        // 写入文件
        InUtils.writeStream(inputStream, "/home/ct/Workspace/java/sdk2x/src/test/fix/config2.json");
        //   Path path = Paths.get("/home/ct/Workspace/java/sdk2x/src/test/fix", "config_block.pb");

        //   Message.Builder configBuilder =    Configtx.Config.newBuilder();

        //  configBuilder.mergeFrom(srcByte);

        // System.out.println(JsonFormat.(configBuilder.build()));

        //  System.out.println( JsonFormat.printer().print(configBuilder));
        // byte[] jsonByte =   JsonFormat.printer().print(configBuilder).getBytes();

        // Rule.writeStream(new ByteArrayInputStream(jsonByte),"/home/ct/Workspace/java/sdk2x/src/test/fix/config2.json");
        // Rule.writeStream(inputStream, path.toString());
        //http://host/protolator/decode/common.Config

        // 读取 org.json
        JSONObject orgJson = readJson("/home/ct/Workspace/java/sdk2x/src/test/fix/org4.json");

        // 文件进行拼接产生新的 modifiedConfig
        String modifiedConfigStr = configJson.toJSONString();
        // jq -s '.[0] * {"channel_group":{"groups":{"Application":{"groups": {"Org3MSP":.[1]}}}}}' config.json ../channel-artifacts/org3.json > modified_config.json
        JSONObject modifiedConfig = JSONObject.parseObject(modifiedConfigStr);
        modifiedConfig.getJSONObject("channel_group").getJSONObject("groups").getJSONObject("Application").getJSONObject("groups").put(orgMSP,orgJson);

        // 把 modifiedConfig 写入文件检测是否写入
        modifiedConfigStr = modifiedConfig.toString();
        InUtils.writeStream("/home/ct/Workspace/java/sdk2x/src/test/fix/modified_config.json",modifiedConfigStr);

        // 把 modifiedConfig 转换成 ProtoBuffer 文件
        byte[] modifiedConfigJsonPB = configtxlatorHttpClient.encodeCommonConfig("47.105.36.27:7059", modifiedConfigStr.getBytes());

        // 写入文件检测
        InputStream isModifiedConfigJsonPB = new ByteArrayInputStream(modifiedConfigJsonPB);
        InUtils.writeStream(isModifiedConfigJsonPB,"/home/ct/Workspace/java/sdk2x/src/test/fix/modified_config.pb");



        String channelName = "mychannel";

        // org4_update.pb
        byte[] orgUpdatePb =  configtxlatorHttpClient.computeUpdate("47.105.36.27:7059",channelName,configPB,modifiedConfigJsonPB);

        InUtils.writeStream(new ByteArrayInputStream(orgUpdatePb),"/home/ct/Workspace/java/sdk2x/src/test/fix/org4_update.pb");

        // 创建 update -f org3_update_in_envelope.pb
        UpdateChannelConfiguration updateChannelConfiguration = new UpdateChannelConfiguration(orgUpdatePb);

        // 创建 OrdererMSP 用户
        OrgManage.UserContext ordererAdmin = om.newUser("Admin", "OrdererMSP");
        // 创建 adminOrg2
        OrgManage.UserContext adminOrg2 = om.newUser("Admin", "Org2MSP", "org2");
        // 创建 adminOrg3
        OrgManage.UserContext adminOrg3 = om.newUser("Admin", "Org3MSP", "org3");
        // 提交签名
        channel.updateChannelConfiguration(updateChannelConfiguration,
                hfClient.getUpdateChannelConfigurationSignature(updateChannelConfiguration,ordererAdmin),
              //  hfClient.getUpdateChannelConfigurationSignature(updateChannelConfiguration,user),
                hfClient.getUpdateChannelConfigurationSignature(updateChannelConfiguration,adminOrg2),
                hfClient.getUpdateChannelConfigurationSignature(updateChannelConfiguration,adminOrg3));

        byte[] configPBNew = channel.getChannelConfigurationBytes();

        // configPB 写入文件检测
        InputStream isConfigPbNew = new ByteArrayInputStream(configPBNew);
        InUtils.writeStream(isConfigPbNew,"/home/ct/Workspace/java/sdk2x/src/test/fix/configAddOrg4.pb");
    }


    public JSONObject readJson(String path) throws FileNotFoundException {
        String org = InUtils.readStream(new FileInputStream(path));
        return  JSONObject.parseObject(org);
    }

}