package com.laiyuezs.sdk2x;

import com.laiyuezs.sdk2x.console.ConsoleCmd;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;


class ConsoleCmdTest {
    @Test
    void pathTest() {
        Path p = Paths.get("/home/ct","Workspace");
        System.out.println(p);
    }

    @Test
    void ConsoleNew(){
        ConsoleCmd consoleCmd = new ConsoleCmd("/home/ct/Workspace/onekey/package/bin/2.2.1");
        System.out.println(consoleCmd.getConfigtxgen().toString());
    }
}