package com.laiyuezs.sdk2x;

import com.alibaba.fastjson.JSONObject;
import org.hyperledger.fabric.sdk.*;
import org.hyperledger.fabric.sdk.exception.*;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

/**
 * @author : 州长在手 2021/8/25 下午5:19
 */
public class TestNewOrg3 {


    String cryptoConfig = "/home/ct/workspace/onekey/outdir";

    @Test
    void lifecycleInstallChaincodeOrg1() throws Exception {
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        OrgManage.UserContext user = om.newUser("Admin", "Org1MSP", "org1");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));

        om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();
        String filePath = "/home/ct/workspace/fabric2.2/fabric-samples/lucia/Admin@org2.example.com/marbles01private082602.tar.gz";
        FileInputStream fileInputStream = new FileInputStream(filePath);
        LifecycleChaincodePackage lifecycleChaincodePackage = ChaincodeLifecycle.newLifecycleChaincodePackage(fileInputStream);

        String label = lifecycleChaincodePackage.getLabel();

        System.out.println(label);

        String packageId =  ChaincodeLifecycle.lifecycleInstallChaincode(channel,hfClient,lifecycleChaincodePackage);

        System.out.println(packageId);
        // marbles01_private_082601_0.1:6f50bec3ff7bd7580007dd3ad435cee57149054bcb7dc7383ee768fbf5bdb0bf
        // marbles01private082602_0.1:90c3a417732e18470460b098eca02a4050a7056362e37df2409afde73994f84f
    }

    @Test
    void lifecycleInstallChaincodeOrg3() throws Exception {
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        OrgManage.UserContext user = om.newUser("Admin", "Org3MSP", "org3");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));

        om.channelAddPeer(om.newPeer("peer0", "org3", "peer0.org3.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();
        String filePath = "/home/ct/workspace/fabric2.2/fabric-samples/lucia/Admin@org2.example.com/marbles01private082602.tar.gz";
        FileInputStream fileInputStream = new FileInputStream(filePath);
        LifecycleChaincodePackage lifecycleChaincodePackage = ChaincodeLifecycle.newLifecycleChaincodePackage(fileInputStream);

        String label = lifecycleChaincodePackage.getLabel();

        System.out.println(label);

        String packageId =  ChaincodeLifecycle.lifecycleInstallChaincode(channel,hfClient,lifecycleChaincodePackage);

        System.out.println(packageId);
        // marbles01_private_082601_0.1:6f50bec3ff7bd7580007dd3ad435cee57149054bcb7dc7383ee768fbf5bdb0bf
        // marbles01private082602_0.1:90c3a417732e18470460b098eca02a4050a7056362e37df2409afde73994f84f
    }

    @Test
    void lifecycleApproveChaincodeOrg1() throws Exception {
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        OrgManage.UserContext user = om.newUser("Admin", "Org1MSP", "org1");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));
        om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();
        String packageID = "marbles01private082602_0.1:90c3a417732e18470460b098eca02a4050a7056362e37df2409afde73994f84f";
        // //Org1 also creates the endorsement policy for the chaincode. // also known as validationParameter !
        LifecycleChaincodeEndorsementPolicy chaincodeEndorsementPolicy = LifecycleChaincodeEndorsementPolicy.fromSignaturePolicyYamlFile(Paths.get("/home/ct/IdeaProjects/fabric-sdk-java/src/test/fixture/" +
                "/sdkintegration/chaincodeendorsementpolicyAllMembers.yaml"));
        ChaincodeCollectionConfiguration chaincodeCollectionConfiguration = ChaincodeCollectionConfiguration.fromYamlFile(new File("/home/ct/go/src/github.com/hyperledger/fabric-samples/chaincode/marbles02_private/collections_config.yaml"));
        String txid = ChaincodeLifecycle.lifecycleApproveChaincode(
                channel,
                hfClient,
                packageID,
                "marbles01private082602",
                "0.1",
                chaincodeCollectionConfiguration,
                chaincodeEndorsementPolicy,
                1 // 失败后要+1
        );
        System.out.println(txid);
    }

    @Test
    void lifecycleApproveChaincodeOrg3() throws Exception {
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        OrgManage.UserContext user = om.newUser("Admin", "Org3MSP", "org3");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));
        om.channelAddPeer(om.newPeer("peer0", "org3", "peer0.org3.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();
        String packageID = "marbles01private082602_0.1:90c3a417732e18470460b098eca02a4050a7056362e37df2409afde73994f84f";
        // //Org1 also creates the endorsement policy for the chaincode. // also known as validationParameter !
        LifecycleChaincodeEndorsementPolicy chaincodeEndorsementPolicy = LifecycleChaincodeEndorsementPolicy.fromSignaturePolicyYamlFile(Paths.get("/home/ct/IdeaProjects/fabric-sdk-java/src/test/fixture/" +
                "/sdkintegration/chaincodeendorsementpolicyAllMembers.yaml"));
        ChaincodeCollectionConfiguration chaincodeCollectionConfiguration = ChaincodeCollectionConfiguration.fromYamlFile(new File("/home/ct/go/src/github.com/hyperledger/fabric-samples/chaincode/marbles02_private/collections_config.yaml"));
        String txid = ChaincodeLifecycle.lifecycleApproveChaincode(
                channel,
                hfClient,
                packageID,
                "marbles01private082602",
                "0.1",
                chaincodeCollectionConfiguration,
                chaincodeEndorsementPolicy,
                1 // 失败后要+1
        );
        System.out.println(txid);
    }

    @Test
    void lifecycleCommitChaincodeOrg1() throws Exception{
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        OrgManage.UserContext user = om.newUser("Admin", "Org1MSP", "org1");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));
        om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();
        // //Org1 also creates the endorsement policy for the chaincode. // also known as validationParameter !
        LifecycleChaincodeEndorsementPolicy chaincodeEndorsementPolicy = LifecycleChaincodeEndorsementPolicy.fromSignaturePolicyYamlFile(Paths.get("/home/ct/IdeaProjects/fabric-sdk-java/src/test/fixture/" +
                "/sdkintegration/chaincodeendorsementpolicyAllMembers.yaml"));
        ChaincodeCollectionConfiguration chaincodeCollectionConfiguration = ChaincodeCollectionConfiguration.fromYamlFile(new File("/home/ct/go/src/github.com/hyperledger/fabric-samples/chaincode/marbles02_private/collections_config.yaml"));
        String txid = ChaincodeLifecycle.lifecycleCommitChaincode(
                channel,
                hfClient,
                "marbles01private082602",
                "0.1",
                true,
                chaincodeCollectionConfiguration,
                chaincodeEndorsementPolicy,
                1 // 失败后加 1
        );
        System.out.println(txid);
    }

    @Test
    void lifecycleInitChaincodeOrg1() throws Exception{
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        OrgManage.UserContext user = om.newUser("Admin", "Org1MSP", "org1");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));
        om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();
        String txid = ChaincodeLifecycle.lifecycleInitChaincode(channel, hfClient, "marbles01private082602", user, "init");
        System.out.println(txid);
    }

    @Test
    void privateDataOrg1() throws Exception {
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        OrgManage.UserContext user = om.newUser("Admin", "Org1MSP", "org1");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));
        om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();

        JSONObject json = new JSONObject();

        json.put("name", "marble1");
        json.put("color", "blue");
        json.put("size", 35);
        json.put("owner", "tom");
        json.put("price", 99);

        Map<String, byte[]> transientMap = new HashMap<>();
        System.out.println(json.toJSONString());

        transientMap.put("marble", json.toJSONString().getBytes());
        JSONObject js = Cc.invoke(hfClient, channel, "marbles01private082602", "initMarble", transientMap);
        System.out.println(js);
    }

    @Test
    void queryOrg1() throws Exception {
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        OrgManage.UserContext user = om.newUser("Admin", "Org1MSP", "org1");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));

        om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        om.build();
        JSONObject  json = Cc.query(om.getHfClient(), om.getChannel(), "marbles01private082602", "readMarble", "marble1");
        System.out.println(json);
        json = Cc.query(om.getHfClient(), om.getChannel(), "marbles01private082602", "readMarblePrivateDetails", "marble1");
        System.out.println(json);
    }

    @Test
    void queryOrg3() throws Exception {
        OrgManage om = new OrgManage(cryptoConfig, "example.com");
        om.initHFClient();
        OrgManage.UserContext user = om.newUser("Admin", "Org3MSP", "org3");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));

        om.channelAddPeer(om.newPeer("peer0", "org3", "peer0.org3.example.com", "7051"));
        om.build();
        JSONObject  json = Cc.query(om.getHfClient(), om.getChannel(), "marbles01private082602", "readMarble", "marble1");
        System.out.println(json);
        json = Cc.query(om.getHfClient(), om.getChannel(), "marbles01private082602", "readMarblePrivateDetails", "marble1");
        System.out.println(json);
    }
}
