package com.laiyuezs.sdk2x.console;

import com.pastdev.jsch.DefaultSessionFactory;
import com.pastdev.jsch.SessionFactory;
import com.pastdev.jsch.command.CommandRunner;
import lombok.Data;

import java.nio.file.Path;
import java.nio.file.Paths;

// 这个类包含所有的二进制命令
@Data
public class ConsoleCmd {

    private String binPath;

    private Path cryptogen;

    private Path configtxgen;

    private Path configtxlator;

    private String jq;

    public  ConsoleCmd(String binPath){
        this.binPath = binPath;
        this.cryptogen =  Paths.get(binPath , "cryptogen");
        this.configtxgen = Paths.get(binPath,"configtxgen");
        this.configtxlator = Paths.get(binPath , "configtxlator");
        this.jq  = "jq";

    }

    // static 形式获取连接对象，只是临时使用
    public static CommandRunner commandRunner(String username, String password, String hostname){
        DefaultSessionFactory defaultSessionFactory = new DefaultSessionFactory(
                username, hostname, SessionFactory.SSH_PORT );
        defaultSessionFactory.setPassword(password);
        // 严格的主机密钥检查
        defaultSessionFactory.setConfig("StrictHostKeyChecking", "no");
        return new CommandRunner( defaultSessionFactory );
    }

}
