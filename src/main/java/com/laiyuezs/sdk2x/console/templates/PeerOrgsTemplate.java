package com.laiyuezs.sdk2x.console.templates;

import com.esotericsoftware.yamlbeans.YamlWriter;
import com.laiyuezs.sdk2x.console.templates.peerOrgTemplate.PeerOrg;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

    /*
PeerOrgs:
  - Name: Org3
    Domain: org3.example.com
    EnableNodeOUs: true
    Template:
      Count: 1
    CA:
        Country: CN
        Province: BJ
        Locality: LY
    Users:
      Count: 1
     */

public class PeerOrgsTemplate implements Template{

    public ArrayList<PeerOrg> PeerOrgs;

    public PeerOrgsTemplate(){
        this.PeerOrgs = new ArrayList<>();
    }

    public void add(PeerOrg peerOrg){
        this.PeerOrgs.add(peerOrg);
    }
    // 直接写入目标指定文件
    public void writeFile(@org.jetbrains.annotations.NotNull Path path) throws IOException {
        YamlWriter yamlWriter = new YamlWriter(new FileWriter(path.toString()));
        yamlWriter.write(this);
        yamlWriter.close();
    }
}
