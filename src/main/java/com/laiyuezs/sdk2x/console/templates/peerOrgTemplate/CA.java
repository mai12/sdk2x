package com.laiyuezs.sdk2x.console.templates.peerOrgTemplate;

/*
    CA:
    Country: CN
    Province: BJ
    Locality: LY
 */

public class CA {

    public String Country ;
    public String Province;
    public String Locality;

    public CA(){}

    public CA(String country,String province,String locality){
        this.Country = country;
        this.Province = province;
        this.Locality = locality;
    }
}
