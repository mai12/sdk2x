package com.laiyuezs.sdk2x.console.templates.organizationsTemplate;

import java.io.*;

public class Rule {
    public static final String SIGNATURE = "Signature";

    public static String adminPeerClient(String mspID) {
        return "OR('" + mspID + ".admin', '" + mspID + ".peer', '" + mspID + ".client')";
    }

    public static String adminClient(String mspID) {
        return "OR('" + mspID + ".admin', '" + mspID + ".client')";
    }

    public static String admin(String mspID) {
        return "OR('" + mspID + ".admin')";
    }


}
