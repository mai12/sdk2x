package com.laiyuezs.sdk2x.console.templates;

import java.nio.file.Path;

public interface Template {
  void   writeFile(Path path) throws Exception;
}
