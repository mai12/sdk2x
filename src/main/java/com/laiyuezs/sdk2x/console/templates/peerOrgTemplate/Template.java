package com.laiyuezs.sdk2x.console.templates.peerOrgTemplate;

/*
    Template:                   ##  允许定义从模板顺序创建的1个或多个主机。 默认情况下，这看起来像是从0到Count-1的“peer”。 您可以覆盖节点数（Count），起始索引（Start）或用于构造名称的模板（Hostname）。
      Count: 2                  ##  表示生成几个Peer
 */
public class Template {
    public int Count ;

    public Template(){}
    public Template(int count){
        this.Count = count;
    }
}
