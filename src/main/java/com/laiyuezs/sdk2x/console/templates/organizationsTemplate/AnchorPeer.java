package com.laiyuezs.sdk2x.console.templates.organizationsTemplate;

public class AnchorPeer {
    public String Host;
    public int  Port;

    public AnchorPeer(){}

    public AnchorPeer(String host,int port){
        this.Host = host;
        this.Port = port;
    }
}
