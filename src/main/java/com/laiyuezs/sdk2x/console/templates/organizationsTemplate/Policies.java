package com.laiyuezs.sdk2x.console.templates.organizationsTemplate;

public class Policies {

    public Readers Readers;
    public Writers Writers;
    public Admins Admins;

    public Policies(){}

    public Policies(Readers readers,Writers writers ,Admins admins){
        this.Readers = readers;
        this.Writers = writers;
        this.Admins = admins;
    }
}
