package com.laiyuezs.sdk2x.console.templates.peerOrgTemplate;

/*
  - Name: Org3
    Domain: org3.example.com
    EnableNodeOUs: true
    Template:
      Count: 2
    CA:
        Country: CN
        Province: BJ
        Locality: LY
    Users:
      Count: 1
 */
public class PeerOrg {
    public String Name;
    public String Domain;
    public boolean EnableNodeOUs;
    public Template Template;
    public CA CA;
    public Users Users;

    public PeerOrg() {

    }

    public PeerOrg setName(String name) {
        this.Name = name;
        return this;
    }

    public PeerOrg setDomain(String domain) {
        this.Domain = domain;
        return this;
    }

    public PeerOrg setEnableNodeOUs(boolean enableNodeOUs){
        this.EnableNodeOUs = enableNodeOUs;
        return this;
    }

    public PeerOrg setTemplate(int count) {
        this.Template = new Template(count);
        return  this;
    }

    public PeerOrg setCA(String country,String province,String locality){
        this.CA = new CA(country,province,locality);
        return this;
    }

    public PeerOrg setUsers(int count){
        this.Users = new Users(count);
        return this;
    }
}
