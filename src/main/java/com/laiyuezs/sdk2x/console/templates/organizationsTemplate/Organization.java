package com.laiyuezs.sdk2x.console.templates.organizationsTemplate;

import java.util.ArrayList;


/*
Organizations:
  - Name: Org3MSP
    ID: Org3MSP
    MSPDir: ../crypto-config/peerOrganizations/org3.example.com/msp
    Policies:
      Readers:
        Type: Signature
        Rule: 'OR(''Org3MSP.admin'', ''Org3MSP.peer'', ''Org3MSP.client'')'
      Writers:
        Type: Signature
        Rule: 'OR(''Org3MSP.admin'', ''Org3MSP.client'')'
      Admins:
        Type: Signature
        Rule: OR('Org3MSP.admin')
    AnchorPeers:
      - Host: peer0.org3.example.com
        Port: 7051
 */
public class Organization {
    public String Name;
    public String ID;
    public String MSPDir;
    public Policies Policies;
    public ArrayList<AnchorPeer> AnchorPeers;

    public Organization() {
        AnchorPeers = new ArrayList<>();
    }

    public void setName(String name) {
        this.Name = name;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public void setMSPDir(String MSPDir) {
        this.MSPDir = MSPDir;
    }

    public void setPolicies(Policies policies) {
        this.Policies = policies;
    }

    public void addAnchorPeer(AnchorPeer anchorPeers) {
        this.AnchorPeers.add(anchorPeers);
    }
}
