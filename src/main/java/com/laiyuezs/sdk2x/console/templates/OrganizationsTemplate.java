package com.laiyuezs.sdk2x.console.templates;

import com.esotericsoftware.yamlbeans.YamlConfig;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;
import com.esotericsoftware.yamlbeans.document.YamlAlias;
import com.laiyuezs.sdk2x.console.templates.organizationsTemplate.Organization;
import com.laiyuezs.sdk2x.console.templates.organizationsTemplate.Rule;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

public class OrganizationsTemplate implements Template {
    public ArrayList<Organization> Organizations;
    public OrganizationsTemplate(){
        this.Organizations = new ArrayList<>();
    }

    public void addOrganization(Organization organization){
        this.Organizations.add(organization);
    }
    // 直接写入目标指定文件
    public void writeFile( @org.jetbrains.annotations.NotNull Path path) throws IOException {
        YamlWriter yamlWriter = new YamlWriter(new FileWriter(path.toString()) );
        yamlWriter.getConfig().writeConfig.setAutoAnchor(false); // 取消添加全限定类名

        yamlWriter.write(this);
        yamlWriter.close();

    }

}
