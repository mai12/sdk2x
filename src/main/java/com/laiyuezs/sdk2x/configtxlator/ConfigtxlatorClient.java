package com.laiyuezs.sdk2x.configtxlator;

import com.alibaba.fastjson.JSONObject;
import com.dtflys.forest.annotation.*;
import com.dtflys.forest.http.ForestRequest;

import java.util.Map;

public interface ConfigtxlatorClient {
    @Post(
            url = "http://${url}/protolator/decode/common.Config",
            contentType = "application/octet-stream"
    )
    JSONObject decodeCommonConfig(@Var("url") String url ,@Body byte[] body, @Var("filename") String filename);

    @Post(
            url = "http://${url}/protolator/encode/common.Config",
            contentType = "application/octet-stream"
    )
    byte[] encodeCommonConfig(@Var("url") String url ,@Body byte[] body, @Var("filename") String filename);


    /**
     * 上传Map包装的文件列表
     * 其中 ${_key} 代表Map中每一次迭代中的键值
     */
    @Post(
            url = "http://${url}/configtxlator/compute/update-from-configs",
            contentType = "application/octet-stream"
    )
    byte[] computeUpdate(@Var("url") String url , @Body("channel") String channel,@DataFile(value = "file", fileName = "${_key}") Map<String, byte[]> byteArrayMap);


}
