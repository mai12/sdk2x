package com.laiyuezs.sdk2x.configtxlator;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dtflys.forest.annotation.Body;
import com.dtflys.forest.annotation.Var;
import com.laiyuezs.sdk2x.InUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;


public class ConfigtxlatorHttpClient {

    HttpClient httpClient;

    public ConfigtxlatorHttpClient(){
        httpClient = HttpClients.createDefault();
    }

    /**
     * @param original config.pb
     * @param updated  modified_config.pb
     */
    public byte[] computeUpdate(String url ,String channelName , byte[] original , byte[]  updated) throws IOException {

        HttpPost httpPost = new HttpPost("http://"+url+"/configtxlator/compute/update-from-configs");
        // 多实体
        HttpEntity multipartEntity = MultipartEntityBuilder.create()
                .setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
                .addBinaryBody("original",original, ContentType.APPLICATION_OCTET_STREAM,"original.proto")
                .addBinaryBody("updated",updated, ContentType.APPLICATION_OCTET_STREAM,"updated.proto")
                .addBinaryBody("channel",channelName.getBytes())
                .build();
        httpPost.setEntity(multipartEntity);

        HttpResponse response = httpClient.execute(httpPost);

        return  EntityUtils.toByteArray(response.getEntity());
    }

    /**
     *
     * @param config config.pb
     * @return json
     */
    public JSONObject decodeCommonConfig(String url , byte[] config) throws IOException {

        HttpPost httpPost = new HttpPost("http://"+url+"/protolator/decode/common.Config");
        // 单实体
        HttpEntity httpEntity = EntityBuilder.create()
                .setBinary(config).build();
        httpPost.setEntity(httpEntity);

        HttpResponse response = httpClient.execute(httpPost);

        return JSONObject.parseObject(EntityUtils.toString(response.getEntity()));
    }

    /**
     *
     * @param modifiedConfig modified_config.json
     * @return pb modified_config
     */
    public byte[]  encodeCommonConfig( String url ,  byte[] modifiedConfig) throws IOException {

        HttpPost httpPost = new HttpPost("http://"+url+"/protolator/encode/common.Config");
        // 单实体
        HttpEntity httpEntity = EntityBuilder.create()
                .setBinary(modifiedConfig).build();
        httpPost.setEntity(httpEntity);

        HttpResponse response = httpClient.execute(httpPost);

        return EntityUtils.toByteArray(response.getEntity());

    }
}
