package com.laiyuezs.sdk2x;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sdk2xApplication {

    public static void main(String[] args) {
        SpringApplication.run(Sdk2xApplication.class, args);
    }

}
