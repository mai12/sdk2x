package com.laiyuezs.sdk2x.external;

import com.alibaba.fastjson.JSONObject;
import com.laiyuezs.sdk2x.configtxlator.ConfigtxlatorHttpClient;

import java.io.IOException;

public class ConfigtxlatorService {

    public  byte[] computeUpdate(String url, String channelName, byte[] configPB , String orgMSP, JSONObject orgJson) throws IOException {

        ConfigtxlatorHttpClient chc = new ConfigtxlatorHttpClient();

        // 调用Forest请求接口，并获取响应返回结果
        JSONObject configJson = chc.decodeCommonConfig(url, configPB);

        // 文件进行拼接产生新的 modifiedConfig
        String modifiedConfigStr = configJson.toJSONString();

        // orgJson 加入到原始config
        JSONObject modifiedConfig = JSONObject.parseObject(modifiedConfigStr);
        modifiedConfig.getJSONObject("channel_group").getJSONObject("groups").
                getJSONObject("Application").getJSONObject("groups").put(orgMSP,orgJson);

        // 把 modifiedConfig 转换成 ProtoBuffer 文件
        byte[] modifiedConfigJsonPB = chc.encodeCommonConfig(url, modifiedConfigStr.getBytes());

        // org_update.pb
        return chc.computeUpdate("47.105.36.27:7059",channelName,configPB,modifiedConfigJsonPB);
    }

}
