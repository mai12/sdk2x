package com.laiyuezs.sdk2x.external;

import com.laiyuezs.sdk2x.OrgManage;

public class OmFactory {


    /**
     * 创建一个OM对象
     * @param cryptoConfigPath 加密材料的路径到crypto-config目录就好了，不需要加 crypto-config
     * @param domain
     * @param org
     * @param orgMspId
     * @param userName
     * @param channelName
     * @param ordererName
     * @param ordererHost
     * @param ordererPort
     * @param peerName
     * @param peerOrg
     * @param peerHost
     * @param peerPort
     * @return
     */
    public static OrgManage  creatOm(String cryptoConfigPath,
                             String domain,
                             String org ,
                             String orgMspId,
                             String userName,
                             String channelName,
                             String ordererName,
                             String ordererHost,
                             String ordererPort,
                             String peerName,
                             String peerOrg,
                             String peerHost,
                             String peerPort) {
        OrgManage om = new OrgManage(cryptoConfigPath, domain);
        om.initHFClient();
        OrgManage.UserContext user = om.newUser(userName, orgMspId, org);
        om.hfClientAddUser(user);
        om.initChannel(channelName);
        om.channelAddOrderer(om.newOrderer(ordererName, ordererHost, ordererPort));
        om.channelAddPeer(om.newPeer(peerName, peerOrg, peerHost, peerPort));
        om.build();
        return om;
    }

    /**
     *  获取用户上下文
     *
     * @param cryptoConfigPath
     * @param domain
     * @param userName
     * @param orgMspId
     * @param org
     * @return
     */
    public static OrgManage.UserContext creatUserContext(String cryptoConfigPath,
                                                  String domain,
                                                  String userName,
                                                  String orgMspId,
                                                  String org){
        OrgManage om = new OrgManage(cryptoConfigPath, domain);
        om.initHFClient();
        return om.newUser(userName, orgMspId, org);
    }
}
