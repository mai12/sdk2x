package com.laiyuezs.sdk2x.external;

import com.laiyuezs.sdk2x.OrgManage;
import lombok.extern.slf4j.Slf4j;
import org.hyperledger.fabric.sdk.UpdateChannelConfiguration;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.TransactionException;

@Slf4j
public class UpdateChannelConfigurationSignature {

    //   private UpdateChannelConfiguration updateChannelConfiguration;


//    public UpdateChannelConfigurationSignature(byte[] orgUpdatePb){
//        // 创建 update -f org3_update_in_envelope.pb
//        try {
//            updateChannelConfiguration = new UpdateChannelConfiguration(orgUpdatePb);
//        } catch (InvalidArgumentException e) {
//            log.error("创建 UpdateChannelConfiguration 对象错误，很可能是传输过来的信封是空或者格式错误： "+e.getMessage());
//        }
//    }


    public byte[] signature(OrgManage om, byte[] orgUpdatePb) {

        UpdateChannelConfiguration updateChannelConfiguration = null;
        try {
            updateChannelConfiguration = new UpdateChannelConfiguration(orgUpdatePb);
        } catch (InvalidArgumentException e) {
            log.error("创建 UpdateChannelConfiguration 对象错误，很可能是传输过来的信封是空或者格式错误： " + e.getMessage());
            return null;
        }

        byte[] signature = null;
        try {
            signature = om.getHfClient().getUpdateChannelConfigurationSignature(updateChannelConfiguration, om.getHfClient().getUserContext());
        } catch (InvalidArgumentException e) {
            log.error("签名错误，检查是否OM创建的问题： " + e.getMessage());
            return null;
        }
        return signature;
    }


    public static   boolean  updateChannel(OrgManage om, byte[] orgUpdatePb , byte[]... signas){

        UpdateChannelConfiguration updateChannelConfiguration = null;
        try {
            updateChannelConfiguration = new UpdateChannelConfiguration(orgUpdatePb);
        } catch (InvalidArgumentException e) {
            log.error("创建 UpdateChannelConfiguration 对象错误，很可能是传输过来的信封是空或者格式错误： " + e.getMessage());
            return false ;
        }

        try {
            // getSignatureList().toArray((byte[]))
            om.getChannel().updateChannelConfiguration(updateChannelConfiguration,signas );
        } catch (TransactionException | InvalidArgumentException e) {
            log.error("updateChannelConfiguration 错误 ： "+ e.getMessage());
            return false;
        }

        return true;
    }

}
