# 工程简介

这个sdk是依赖官方的2.2.1,就是对sdk使用进行简化,通过加载 crypto-config 目录,
做到灵活选取身份,对fabric进行统一管理

# 延伸阅读

简单使用

```
        // 加载加密材料
        OrgManage om = new OrgManage("/home/ct/workspace/fabric2.2/fabric-samples/lucia", "example.com");
        om.initHFClient();
        // 选择身份
        OrgManage.UserContext user = om.newUser("Admin", "Org1MSP", "org1");
        om.hfClientAddUser(user);
        om.initChannel("mychannel");
        // 添加链接的orderer,可以添加多个
        om.channelAddOrderer(om.newOrderer("orderer", "orderer.example.com", "7050"));
        // 添加链接的peer,可以添加多个
        om.channelAddPeer(om.newPeer("peer0", "org1", "peer0.org1.example.com", "7051"));
        om.build();
        Channel channel = om.getChannel();
        HFClient hfClient = om.getHfClient();
        System.out.println(channel.queryBlockchainInfo().getHeight());

```

详细使用请看  TEST

```

        Cc类主要是做query和invoke,其中invoke是阻塞的为了获取数据保存的区块高度
        Cl类是对链的查询,比如根据hash查区块等,注意,打印的区块hash是被16进制编码的字符串
        需要解码为byte数组

```

